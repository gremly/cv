TEX = lualatex
src = cv.tex

pdf : $(src)
	$(TEX) $(src) && $(TEX) $(src)

clean :
	rm *.pdf *.aux *.log *.out
